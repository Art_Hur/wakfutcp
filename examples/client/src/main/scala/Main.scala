import java.net.InetSocketAddress

import akka.actor._
import akka.actor.typed.scaladsl.adapter._
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import wakfutcp.client.util.Auth.Credentials
import wakfutcp.client.util.GameSession
import wakfutcp.protocol.common.Version

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object Main {
  def main(args: Array[String]): Unit = {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    val config = ConfigFactory.load()

    val version = {
      val v = config.getString("auth.version").split('.')
      Version(v(0).toByte, v(1).toShort, v(2).toByte)
    }
    val address = new InetSocketAddress(config.getString("auth.address"), config.getInt("auth.port"))
    val credentials = Credentials(config.getString("account.login"), config.getString("account.password"))

    val behavior = GameSession.behavior(address, credentials, version, MarketSniffer.behavior) { (servers) =>
      servers.collectFirst { case (proxy, _) if proxy.name == "Remington" => proxy }.get
    }

    system.spawn(behavior, "GameSession")

    Await.result(system.whenTerminated, Duration.Inf)
    ()
  }
}
