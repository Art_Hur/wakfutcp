import java.nio.ByteBuffer
import java.security.{PrivateKey, PublicKey}
import java.util.UUID

import akka.actor.ActorSystem
import akka.actor.typed.scaladsl._
import akka.actor.typed.scaladsl.adapter._
import akka.actor.typed.{ActorRef, Behavior}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Tcp
import akka.util.ByteString
import shapeless._
import wakfutcp.protocol.common._
import wakfutcp.protocol.messages.client.ClientDispatchAuthenticationMessage.CredentialData
import wakfutcp.protocol.messages.client._
import wakfutcp.protocol.messages.server._
import wakfutcp.protocol.protobuf.account.Status
import wakfutcp.protocol.raw.character.ForCharacterList
import wakfutcp.protocol.util.extensions.server._
import wakfutcp.protocol.util.stream
import wakfutcp.protocol.util.stream.Recv
import wakfutcp.protocol.{ClientMessage, Decoder}

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.reflect.io.Streamable
import scala.util.Random

object Main extends App {
  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val salt: Long = new Random().nextLong()
  val pubKey: PublicKey = getPublic
  val privKey: PrivateKey = getPrivate

  val proxies =
    Array(Proxy(0, "Kokokobana", Community.UK, ProxyServer("localhost", Array(8081)), 0))

  val worlds =
    Array(WorldInfo(0, Version.WithBuild(Version(1, 58, 2), "-1"), Array(), locked = false))

  val character: ForCharacterList = {
    import wakfutcp.protocol.raw.CharacterData._

    Id(1234567891011L) ::
      Identity(0, 87409751) ::
      Name("THIS IS JOHN CEEEEENA") ::
      Breed(3) ::
      ActiveEquipmentSheet(0) ::
      Appearance(0, 8, 19, 2, -3, 0, 1, 2, -1) ::
      EquipmentAppearance(tag
        .apply[Byte]
        .apply[Array[EquipmentAppearanceEquipmentAppearance]](Array())) :: // this is temporary...
      CreationData(
        Some(
          CreationDataCreationData(
            newCharacter = false,
            needsRecustom = false,
            0,
            needInitialXp = false
          )
        )
      ) ::
      Xp(99999999999999L) ::
      NationId(30) ::
      GuildId(-1) ::
      GuildBlazon(1688849861181465L) ::
      InstanceId(131) ::
      HNil
  }

  Tcp()
    .bind("localhost", 8080)
    .runForeach { conn =>
      conn.handleWith(stream.server.flow(system.spawn(stream.withConnection(auth), "Auth")))
    }

  Tcp()
    .bind("localhost", 8081)
    .runForeach { conn =>
      conn.handleWith(stream.server.flow(system.spawn(stream.withConnection(world), "World")))
    }

  Await.result(system.whenTerminated, Duration.Inf)
  ()


  def auth(connection: ActorRef[ByteString]): Behavior[stream.Message[ClientMessage]] =
    Behaviors.immutable { (_, msg) =>
      msg match {
        case Recv(ClientVersionMessage(v)) =>
          connection !! ClientVersionResultMessage(success = true, v.version)
        case Recv(ClientPublicKeyRequestMessage(_)) =>
          connection !! ClientPublicKeyMessage(salt, pubKey.getEncoded)
        case Recv(ClientDispatchAuthenticationMessage(encryptedCredentials)) =>
          import javax.crypto.Cipher

          val decrypt = Cipher.getInstance("RSA")
          decrypt.init(Cipher.DECRYPT_MODE, privKey)
          val decryptedMessage = decrypt.doFinal(encryptedCredentials)
          val data = Decoder[CredentialData].decode(ByteBuffer.wrap(decryptedMessage))
          println(s"your login was ${data.login}")

          connection !! ClientDispatchAuthenticationResultMessage(
            ClientDispatchAuthenticationResultMessage.Result.Success,
            Some(AccountInformation(Community.UK, None, Status(Map()))))
        case Recv(ClientProxiesRequestMessage()) =>
          connection !! ClientProxiesResultMessage(proxies, worlds)
        case Recv(AuthenticationTokenRequestMessage(id, aid)) =>
          connection !! AuthenticationTokenResultMessage.Success(UUID.randomUUID().toString)
        case _ =>
      }
      Behaviors.same
    }

  def world(connection: ActorRef[ByteString]): Behavior[stream.Message[ClientMessage]] =
    Behaviors.immutable { (_, msg) =>
      msg match {
        case Recv(ClientVersionMessage(v)) =>
          connection !! ClientVersionResultMessage(success = true, v.version)
        case Recv(ClientPublicKeyRequestMessage(_)) =>
          connection !! ClientPublicKeyMessage(salt, pubKey.getEncoded)
        case Recv(ClientAuthenticationTokenMessage(tok)) =>
          connection !! ClientAuthenticationResultsMessage.Success(Array.empty)
          connection !! WorldSelectionResultMessage.Success
          connection !! CharactersListMessage(Array(character))
        case _ =>
      }
      Behaviors.same
    }

  def getPrivate: PrivateKey = {
    import java.security.KeyFactory
    import java.security.spec.PKCS8EncodedKeySpec


    val keyBytes = Streamable.bytes(getClass.getResourceAsStream("/private_key.der"))
    val spec = new PKCS8EncodedKeySpec(keyBytes)
    val kf = KeyFactory.getInstance("RSA")
    kf.generatePrivate(spec)
  }

  def getPublic: PublicKey = {
    import java.security.KeyFactory
    import java.security.spec.X509EncodedKeySpec

    val keyBytes = Streamable.bytes(getClass.getResourceAsStream("/public_key.der"))
    val spec = new X509EncodedKeySpec(keyBytes)
    val kf = KeyFactory.getInstance("RSA")
    kf.generatePublic(spec)
  }
}
