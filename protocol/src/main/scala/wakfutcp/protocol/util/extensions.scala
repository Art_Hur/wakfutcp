package wakfutcp.protocol.util

import java.nio.ByteBuffer

import akka.actor.typed.ActorRef
import akka.util.{ByteString, ByteStringBuilder}
import wakfutcp.protocol.{ClientMessage, Decoder, Encoder, ServerMessage}

object extensions {
  object client {
    implicit final class ActorRefMessageWriter(val ref: ActorRef[ByteString]) extends AnyVal {
      def !![T <: ClientMessage: Encoder](msg: T): Unit =
        ref ! encodeClientMessage(msg)
    }

    private def encodeClientMessage[T <: ClientMessage: Encoder](msg: T): ByteString = {
      import Encoder.DefaultOrder

      val bs = ByteString.newBuilder.putEncodable(msg).result()
      val length = ClientMessage.HeaderSize + bs.length
      val builder = ByteString.newBuilder
      builder.sizeHint(length)
      builder
        .putShort(length.toInt)
        .putByte(msg.arch)
        .putShort(msg.id.toInt)
        .append(bs)
        .result()
    }
  }

  object server {
    implicit final class ActorRefMessageWriter(val ref: ActorRef[ByteString]) extends AnyVal {
      def !![T <: ServerMessage: Encoder](msg: T): Unit =
        ref ! encodeServerMessage(msg)
    }

    private def encodeServerMessage[T <: ServerMessage: Encoder](msg: T): ByteString = {
      import Encoder.DefaultOrder

      val bs = ByteString.newBuilder.putEncodable(msg).result()
      val length = ServerMessage.HeaderSize + bs.length
      val builder = ByteString.newBuilder
      builder.sizeHint(length)
      builder
        .putShort(length.toInt)
        .putShort(msg.id.toInt)
        .append(bs)
        .result()
    }
  }

  implicit final class ByteBufferExt(val bb: ByteBuffer) extends AnyVal {
    @inline def getDecodable[T: Decoder]: T =
      implicitly[Decoder[T]].decode(bb)

    def getUTF8(size: Int): String =
      new String(getByteArray(size), "UTF-8")

    @inline def getUTF8_32: String = getUTF8(bb.getInt)

    @inline def getUTF8_16: String = getUTF8(bb.getShort.toInt)

    @inline def getUTF8_u16: String = getUTF8(bb.getShort & 0xFFFF)

    @inline def getUTF8_8: String = getUTF8(bb.get.toInt)

    @inline def getUTF8_u8: String = getUTF8(bb.get & 0xFF)

    def getByteArray(size: Int): Array[Byte] = {
      val data = new Array[Byte](size)
      bb.get(data)
      data
    }

    @inline def getByteArray_32: Array[Byte] = getByteArray(bb.getInt)

    @inline def getByteArray_16: Array[Byte] = getByteArray(bb.getShort.toInt)

    @inline def getByteArray_u16: Array[Byte] =
      getByteArray(bb.getShort & 0xFFFF)

    @inline def getByteArray_8: Array[Byte] = getByteArray(bb.get.toInt)

    @inline def getByteArray_u8: Array[Byte] = getByteArray(bb.get & 0xFF)

    def getRemainingBytes: Array[Byte] = {
      val data = new Array[Byte](bb.remaining)
      bb.get(data)
      data
    }

    // optimized version without copying
    def getBlockUsing[T](size: Int, fn: ByteBuffer => T): T = {
      val slice = bb.slice()
      bb.position(bb.position + size)
      slice.limit(size)
      fn(slice)
    }

    @inline def getBlockUsing_32[T](fn: ByteBuffer => T): T =
      getBlockUsing(bb.getInt, fn)

    @inline def getBlockUsing_16[T](fn: ByteBuffer => T): T =
      getBlockUsing(bb.getShort, fn)

    @inline def getBlockUsing_u16[T](fn: ByteBuffer => T): T =
      getBlockUsing(bb.getShort & 0xFFFF, fn)

    @inline def getBlockUsing_8[T](fn: ByteBuffer => T): T =
      getBlockUsing(bb.get, fn)

    @inline def getBlockUsing_u8[T](fn: ByteBuffer => T): T =
      getBlockUsing(bb.get & 0xFF, fn)
  }

  implicit final class ByteStringBuilderExt(val builder: ByteStringBuilder) extends AnyVal {
    import Encoder.DefaultOrder

    @inline def putEncodable[T: Encoder](t: T): ByteStringBuilder =
      implicitly[Encoder[T]].encode(builder, t)

    @inline def putBoolean(boolean: Boolean): ByteStringBuilder =
      builder.putByte(if (boolean) 1 else 0)

    def putUTF8(string: String): ByteStringBuilder =
      builder.putBytes(string.getBytes("UTF-8"))

    @inline def putUTF8_32(str: String): ByteStringBuilder =
      builder.putInt(str.length).putUTF8(str)

    @inline def putUTF8_16(str: String): ByteStringBuilder =
      builder.putShort(str.length).putUTF8(str)

    @inline def putUTF8_u16(str: String): ByteStringBuilder =
      builder.putShort(str.length & 0xFFFF).putUTF8(str)

    @inline def putUTF8_8(str: String): ByteStringBuilder =
      builder.putByte(str.length.toByte).putUTF8(str)

    @inline def putUTF8_u8(str: String): ByteStringBuilder =
      builder.putByte(str.length & 0xFF toByte).putUTF8(str)

    @inline def putByteArray_32(data: Array[Byte]): ByteStringBuilder =
      builder.putInt(data.length).putBytes(data)

    @inline def putByteArray_16(data: Array[Byte]): ByteStringBuilder =
      builder.putShort(data.length).putBytes(data)

    @inline def putByteArray_u16(data: Array[Byte]): ByteStringBuilder =
      builder.putShort(data.length & 0xFFFF).putBytes(data)

    @inline def putByteArray_8(data: Array[Byte]): ByteStringBuilder =
      builder.putByte(data.length.toByte).putBytes(data)

    @inline def putByteArray_u8(data: Array[Byte]): ByteStringBuilder =
      builder.putByte(data.length & 0xFF toByte).putBytes(data)

    @inline def putBlock_32[T](bs: ByteString): ByteStringBuilder =
      builder putInt bs.length ++= bs

    @inline def putBlock_16[T](bs: ByteString): ByteStringBuilder =
      builder putShort bs.length ++= bs

    @inline def putBlock_u16[T](bs: ByteString): ByteStringBuilder =
      builder putShort (bs.length & 0xFFFF) ++= bs

    @inline def putBlock_8[T](bs: ByteString): ByteStringBuilder =
      builder putByte bs.length.toByte ++= bs

    @inline def putBlock_u8[T](bs: ByteString): ByteStringBuilder =
      builder putByte (bs.length & 0xFF).toByte ++= bs
  }
}
