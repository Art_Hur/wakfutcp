package wakfutcp.protocol

import java.nio.ByteBuffer

import akka.util.ByteStringBuilder
import cats.syntax.invariant._
import shapeless._
import shapeless.tag.@@

import scala.reflect.ClassTag

object generic {
  type I8Array[T] = Array[T] @@ Byte
  type I8String = String @@ Byte

  implicit val bool: Codec[Boolean] = Codec.bool
  implicit val byte: Codec[Byte] = Codec.byte
  implicit val short: Codec[Short] = Codec.short
  implicit val int: Codec[Int] = Codec.int
  implicit val long: Codec[Long] = Codec.long
  implicit val float: Codec[Float] = Codec.float
  implicit val double: Codec[Double] = Codec.double
  implicit val utf8: Codec[String] = Codec.utf8(Codec.ushort)
  implicit val i8utf8: Codec[I8String] =
    Codec.utf8(Codec.ubyte).imap(_.asInstanceOf[I8String])(identity)

  implicit val byteArray: Codec[Array[Byte]] = Codec.byteArray(Codec.ushort)
  implicit val i8byteArray: Codec[I8Array[Byte]] =
    Codec.byteArray(Codec.ubyte).imap(_.asInstanceOf[I8Array[Byte]])(identity)

  implicit def itemArray[T: Codec: ClassTag]: Codec[Array[T]] =
    Codec.array(Codec.ushort, Codec[T])
  implicit def i8itemArray[T: Codec: ClassTag]: Codec[I8Array[T]] =
    Codec.array(Codec.ubyte, Codec[T]).imap(_.asInstanceOf[I8Array[T]])(identity)

  implicit def option[T: Codec]: Codec[Option[T]] =
    Codec.option[T](Codec[T])

  implicit def deriveHNil: Codec[HNil] =
    new Codec[HNil] {
      override def decode(bb: ByteBuffer): HNil = HNil
      override def encode(builder: ByteStringBuilder, t: HNil): ByteStringBuilder = builder
    }

  implicit def deriveHCons[V, T <: HList](implicit scv: Lazy[Codec[V]], sct: Lazy[Codec[T]]): Codec[V :: T] =
    new Codec[V :: T] {
      override def decode(bb: ByteBuffer): V :: T = scv.value.decode(bb) :: sct.value.decode(bb)
      override def encode(builder: ByteStringBuilder, t: V :: T): ByteStringBuilder = {
        scv.value.encode(builder, t.head)
        sct.value.encode(builder, t.tail)
        builder
      }
    }

  implicit def deriveClass[A, R](implicit gen: Generic.Aux[A, R], conv: Codec[R]): Codec[A] =
    new Codec[A] {
      override def decode(bb: ByteBuffer): A = gen.from(conv.decode(bb))
      override def encode(builder: ByteStringBuilder, t: A): ByteStringBuilder = conv.encode(builder, gen.to(t))
    }
}

