package wakfutcp.protocol

import java.nio.ByteBuffer

import akka.util.{ByteString, ByteStringBuilder}

trait BinarSerialPart {
  def id: Byte
}

trait BinarSerialCodec[B <: BinarSerialPart] extends Codec[Seq[_ <: B]] {
  import Encoder.DefaultOrder

  def fromId(id: Byte): Codec[_ <: B]

  final override def decode(bb: ByteBuffer): Seq[_ <: B] = {
    val parts = Seq.fill(bb.get)((bb.get, bb.getInt))
    parts.zipWithIndex.map {
      case ((idx, offset), i) =>
        val size =
          if (i < parts.length - 1) {
            parts(i + 1)._2 - offset - 1
          } else {
            bb.limit - offset - 1
          }
        bb.position(offset + 1)
        val slice = bb.slice()
        slice.limit(size)
        fromId(idx).decode(slice)
    }
  }

  @SuppressWarnings(Array("org.wartremover.warts.AsInstanceOf"))
  final override def encode(
    builder: ByteStringBuilder,
    t: Seq[_ <: B]
  ): ByteStringBuilder = {
    builder.putByte(t.length.toByte)

    val headerSize = 1 + 5 * t.length

    val content = ByteString.newBuilder
    t.map { part =>
      val offset = content.length
      val codec = fromId(part.id)
      content.putByte(part.id)
      codec.asInstanceOf[Codec[B]].encode(content, part)
      (part.id, offset)
    }.foreach {
      case (idx, offset) =>
        builder.putByte(idx)
        builder.putInt(headerSize + offset)
    }
    builder ++= content.result()
  }
}

object BinarSerialCodec {
  @inline def apply[T <: BinarSerialPart: BinarSerialCodec]: BinarSerialCodec[T] =
    implicitly[BinarSerialCodec[T]]
}
