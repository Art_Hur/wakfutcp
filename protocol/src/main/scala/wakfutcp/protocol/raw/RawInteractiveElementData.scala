package wakfutcp.protocol.raw

trait RawInteractiveElementData extends RawData

final case class RawInteractiveElementPersistantData(
  templateId: Long,
  positionX: Int,
  positionY: Int,
  positionZ: Short,
  direction: Byte,
  itemForm: RawInventoryItem,
  specificData: Byte // virtual
) extends RawInteractiveElementData

final case class RawItemizableSynchronisationData(
  positionX: Int,
  positionY: Int,
  positionZ: Short,
  direction: Byte,
  ownerId: Long
) extends RawInteractiveElementData

final case class RawPersistantDataNone() extends RawInteractiveElementData

final case class RawPersistantDataMerchantDisplay(
  content: RawMerchantItemInventory
) extends RawInteractiveElementData

final case class RawPersistantEquipableDummy(
  content: RawEquipableDummy
) extends RawInteractiveElementData

final case class RawPersistantBookcase(
  content: RawBookcase
) extends RawInteractiveElementData

final case class RawZaapOutTravelMachine(
  guildId: Long
) extends RawInteractiveElementData
