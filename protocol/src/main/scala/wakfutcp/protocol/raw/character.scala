package wakfutcp.protocol.raw

import java.nio.ByteBuffer

import akka.util.ByteStringBuilder
import cats.instances.byte._
import cats.syntax.eq._
import shapeless.{::, HList, HNil, Lazy}
import wakfutcp.protocol.Codec
import wakfutcp.protocol.generic._
import wakfutcp.protocol.raw.CharacterData._

object character {
  type ForCharacterList =
    Id :: Identity :: Name :: Breed :: ActiveEquipmentSheet :: Appearance :: EquipmentAppearance :: CreationData :: Xp :: NationId :: GuildId :: GuildBlazon :: InstanceId :: HNil
  implicit val forCharacterListCodec: Codec[ForCharacterList] = mkCodec(4)

  type ForLocalCharacterInformation =
    Id :: Identity :: Name :: Breed :: GuildBlazon :: GuildId :: Hp :: Position :: Appearance :: ShortcutInventories :: EmoteInventory :: LandMarkInventory :: DiscoveredItemsInventory :: SpellInventory :: Inventories :: Bags :: ProtoTemporaryInventory :: BreedSpecific :: SkillInventory :: Craft :: RunningEffects :: DimensionalBagForClient :: Challenges :: Xp :: XpCharacteristics :: Title :: NationCitizenScore :: PassportInfo :: SocialStates :: Pet :: Achievements :: AccountInformation :: LocksForClient :: DimensionalBagViewInventory :: PersonalEffects :: AntiAddiction :: WorldProperties :: Visibility :: Occupation :: SpellDeck :: DungeonProgression :: HNil
  implicit val forLocalCharacterInformationCodec: Codec[ForLocalCharacterInformation] = mkCodec(8)

  type ForCharacterPositionInformation =
    Id :: Identity :: Position :: HNil
  implicit val forCharacterPositionInformationCodec: Codec[ForCharacterPositionInformation] = mkCodec(26)

  private def mkCodec[T, V <: HList](id: Byte)(implicit scv: Lazy[Codec[V]], sct: Lazy[Codec[T]]): Codec[T :: V] =
    new Codec[T :: V] {
      override def decode(bb: ByteBuffer): T :: V = {
        assert(bb.get === id)
        deriveHCons[T, V].decode(bb)
      }

      override def encode(builder: ByteStringBuilder, t: ::[T, V]): ByteStringBuilder = {
        builder.putByte(id)
        deriveHCons[T, V].encode(builder, t)
      }
    }
}

