package wakfutcp.protocol

import wakfutcp.protocol.generic._
import wakfutcp.protocol.raw.CharacterData.{AccountInformation, DiscoveredItemsInventory, Properties}

package object raw {
  // those help deriving generic codecs by caching some of them
  implicit val rawPetCodec: Codec[RawPet] = deriveClass
  implicit val rawInventoryItemCodec: Codec[RawInventoryItem] = deriveClass
  implicit val bagsCodec: Codec[RawBag] = deriveClass
  implicit val rawSpellLevelCodec: Codec[RawSpellLevel] = deriveClass
  implicit val dimensionalBagForClientCodec: Codec[RawDimensionalBagForClient] = deriveClass
  implicit val inventoriesCodec: Codec[RawInventoryHandler] = deriveClass
  implicit val challengesCodec: Codec[RawScenario] = deriveClass
  implicit val accountInformationCodec: Codec[AccountInformation] = deriveClass
  implicit val propertiesCodec: Codec[Properties] = deriveClass
  implicit val characteristicsCodec: Codec[RawBonusPointCharacteristics] = deriveClass
  implicit val discoveredItemsInventoryCodec: Codec[DiscoveredItemsInventory] = deriveClass
  implicit val rawSymbiotCodec: Codec[RawSymbiot] = deriveClass
  implicit val rawSkillInventoryCodec: Codec[RawSkill] = deriveClass
}
