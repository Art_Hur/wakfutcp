package wakfutcp.protocol.raw

import java.nio.ByteBuffer

import akka.util.ByteStringBuilder
import wakfutcp.protocol.Codec
import wakfutcp.protocol.generic._

object virtual {
  final case class RawSpecificRooms(member: RawSpecificRoomsMember)

  object RawSpecificRooms {
    implicit val codec: Codec[RawSpecificRooms] =
      new Codec[RawSpecificRooms] {
        override def decode(bb: ByteBuffer): RawSpecificRooms =
          bb.get match {
            case 0 =>
              RawSpecificRooms(
                Codec[RawGemControlledRoom].decode(bb)
              )
            case _ =>
              RawSpecificRooms(NullRoom)
          }

        override def encode(
          builder: ByteStringBuilder,
          t: RawSpecificRooms
        ): ByteStringBuilder = t.member match {
          case raw: RawGemControlledRoom =>
            builder.putByte(0)
            Codec[RawGemControlledRoom].encode(builder, raw)
          case NullRoom =>
            builder.putByte(1)
        }
      }
  }

  sealed trait RawSpecificRoomsMember

  final case class RawGemControlledRoom(
    primaryGemLocked: Boolean,
    primaryGemitemRefId: Int,
    primaryGemUniqueId: Long,
    secondaryGemitemRefId: Int,
    secondaryGemUniqueId: Long
  ) extends RawSpecificRoomsMember

  case object NullRoom extends RawSpecificRoomsMember
}

