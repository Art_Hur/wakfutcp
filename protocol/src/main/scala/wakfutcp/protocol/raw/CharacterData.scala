package wakfutcp.protocol.raw

import wakfutcp.protocol.generic._

trait CharacterData extends RawData

object CharacterData {
  final case class ActiveEquipmentSheet(
    index: Byte
  ) extends CharacterData

  final case class Appearance(
    sex: Byte,
    skinColorIndex: Byte,
    hairColorIndex: Byte,
    pupilColorIndex: Byte,
    skinColorFactor: Byte,
    hairColorFactor: Byte,
    clothIndex: Byte,
    faceIndex: Byte,
    currentTitle: Short
  ) extends CharacterData

  final case class Inventories(
    inventoryHandler: RawInventoryHandler
  ) extends CharacterData

  final case class Cosmetics(
    cosmeticsInventoryHandler: RawCosmeticsInventoryHandler
  ) extends CharacterData

  final case class SpellInventory(
    spellInventory: RawSpellLevelInventory,
    needSpellRestat: Boolean,
    needSpellAutoRestat: Boolean
  ) extends CharacterData

  final case class SkillInventory(
    skillInventory: RawSkillInventory
  ) extends CharacterData

  final case class Craft(
    rawCrafts: Array[CraftRawCraft]
  ) extends CharacterData

  final case class CraftCounter(
    plantationCounter: Option[CraftCounterPlantationCounter],
    nonDestructiveCollectCounter: Option[CraftCounterNonDestructiveCollectCounter],
    destructiveCollectCounter: Option[CraftCounterDestructiveCollectCounter],
    recipeCounter: Option[CraftCounterRecipeCounter]
  ) extends CharacterData

  final case class Pet(
    pet: Option[PetPet],
    mount: Option[PetMount]
  ) extends CharacterData

  final case class RunningEffectsForSave(
    stateRunningEffects: RawStateRunningEffects
  ) extends CharacterData

  final case class Hp(
    hp: Int
  ) extends CharacterData

  final case class Visibility(
    visible: Boolean
  ) extends CharacterData

  final case class Xp(
    xp: Long
  ) extends CharacterData

  final case class XpCharacteristics(
    bonusPointCharacteristics: RawBonusPointCharacteristics,
    wakfuGauge: Int
  ) extends CharacterData

  final case class NationCitizenScore(
    nationCitizenScores: Array[NationCitizenScoreNationCitizenScoreInfo],
    offendedNations: Array[NationCitizenScoreOffendedNations]
  ) extends CharacterData

  final case class PasseportInfo(
    isPasseportActive: Boolean
  ) extends CharacterData

  final case class EquipmentInventory(
    equipment: RawInventoryItemInventory
  ) extends CharacterData

  final case class DimensionalBagForSave(
    dimensionalBag: Option[DimensionalBagForSaveDimensionalBag]
  ) extends CharacterData

  final case class Id(
    id: Long
  ) extends CharacterData

  final case class Identity(
    `type`: Byte,
    ownerId: Long
  ) extends CharacterData

  final case class Breed(
    breedId: Short
  ) extends CharacterData

  final case class Sex(
    sex: Byte
  ) extends CharacterData

  final case class Position(
    x: Int,
    y: Int,
    z: Short,
    instanceId: Short,
    direction: Byte,
    dimBagPosition: Option[PositionDimBagPosition]
  ) extends CharacterData

  final case class PreviousPosition(
    valid: Boolean,
    x: Int,
    y: Int,
    z: Short,
    instanceId: Short
  ) extends CharacterData

  final case class Name(
    name: String
  ) extends CharacterData

  final case class Bags(
    bagContainer: RawBagContainer
  ) extends CharacterData

  final case class ProtoBags(
    data: Array[Byte]
  ) extends CharacterData

  final case class ProtoTemporaryInventory(
    data: Array[Byte]
  ) extends CharacterData

  final case class CreationData(
    creationData: Option[CreationDataCreationData]
  ) extends CharacterData

  final case class BreedSpecific(
    osaSpecific: Option[BreedSpecificOsaSpecific]
  ) extends CharacterData

  final case class RespawnPoint(
    x: Int,
    y: Int,
    z: Short,
    instanceId: Short
  ) extends CharacterData

  final case class ScenarioManager(
    scenarioManager: RawScenarioManager
  ) extends CharacterData

  final case class Title(
    availableTitles: Array[TitleTitleAvalability]
  ) extends CharacterData

  final case class GameServer(
    gameServer: I8String,
    lastLog: Long
  ) extends CharacterData

  final case class Guild(
    guildId: Long,
    fightXp: Long,
    jobXP: Long,
    rank: Byte
  ) extends CharacterData

  final case class NationStorage(
    jobs: Long,
    voteDate: Long,
    governmentOpinion: Byte,
    pvpState: Byte,
    pvpDate: Long,
    pvpRank: Byte,
    pvpMoneyAmount: Long,
    dailyPvpMoneyAmount: Long,
    dailyPvpMoneyStartDate: Long
  ) extends CharacterData

  final case class EmoteInventory(
    emotes: Array[EmoteInventoryEmote]
  ) extends CharacterData

  final case class DimensionalBagViewInventory(
    views: Array[DimensionalBagViewInventoryView]
  ) extends CharacterData

  final case class DiscoveredItemsInventory(
    zaaps: Array[DiscoveredItemsInventoryZaap],
    dragos: Array[DiscoveredItemsInventoryDrago],
    boats: Array[DiscoveredItemsInventoryBoat],
    cannon: Array[DiscoveredItemsInventoryCannon],
    phoenix: Array[DiscoveredItemsInventoryPhoenix],
    selectedPhoenix: Int
  ) extends CharacterData

  final case class Statistics(
    earnedKamas: Int,
    lostKamas: Int,
    fightCounter: Short,
    deathCounter: Short,
    achievementDates: Array[StatisticsAchievementDate],
    killCounters: Array[StatisticsKillCounter],
    collectCounters: Array[StatisticsCollectCounter],
    plantCounters: Array[StatisticsPlantCounter],
    craftCounters: Array[StatisticsCraftCounter],
    dropCounters: Array[StatisticsDropCounter],
    marketBuyCount: Array[StatisticsMarketCounter]
  ) extends CharacterData

  final case class NationSynchro(
    rank: Long,
    jobs: Long,
    voteDate: Long,
    governmentOpinion: Byte,
    isCandidate: Boolean,
    pvpState: Byte,
    pvpDate: Long,
    pvpRank: Byte
  ) extends CharacterData

  final case class NationPvp(
    pvpState: Byte,
    pvpDate: Long,
    pvpRank: Byte,
    pvpMoneyAmount: Long,
    dailyPvpMoneyAmount: Long,
    dailyPvpMoneyStartDate: Long
  ) extends CharacterData

  final case class SpellDeck(
    spellDeckSet: Array[SpellDeckSpellDeck],
    size: Int
  ) extends CharacterData

  final case class NationPvpMoney(
    pvpMoneyAmount: Long,
    dailyPvpMoneyAmount: Long
  ) extends CharacterData

  final case class Group(
    partyId: Long
  ) extends CharacterData

  final case class GuildId(
    guildId: Long
  ) extends CharacterData

  final case class DownscaleInfo(
    downscaleLevel: Short
  ) extends CharacterData

  final case class GuildBlazon(
    guildBlazon: Long
  ) extends CharacterData

  final case class InstanceId(
    instanceId: Short
  ) extends CharacterData

  final case class HeroInfo(
    isHero: Boolean
  ) extends CharacterData

  final case class GuildInfoForGame(
    guildId: Long,
    authorisations: Long,
    activeBuffs: Array[Int]
  ) extends CharacterData

  final case class PersonalEffects(
    guildEffects: Array[Int],
    havenWorldEffects: Array[Int],
    antiAddictionEffects: Array[Int]
  ) extends CharacterData

  final case class RemoteGuildInfo(
    guildId: Long,
    blazon: Long,
    level: Short,
    guildName: String,
    nationId: Int
  ) extends CharacterData

  final case class LocalGuildInfo(
    guild: Array[Byte],
    havenWorldId: Int,
    moderationBonusLearningFactor: Float
  ) extends CharacterData

  final case class NationId(
    nationId: Int
  ) extends CharacterData

  final case class Challenges(
    challenges: RawScenarioManager
  ) extends CharacterData

  final case class DimensionalBagForClient(
    bag: RawDimensionalBagForClient
  ) extends CharacterData

  final case class ShortcutInventories(
    shortcutInventories: Array[ShortcutInventoriesShortcutInventory]
  ) extends CharacterData

  final case class RunningEffects(
    inFightData: Option[RunningEffectsInFightData],
    outFightData: Option[RunningEffectsOutFightData]
  ) extends CharacterData

  final case class CurrentMovementPath(
    currentPath: Option[CurrentMovementPathCurrentPath]
  ) extends CharacterData

  final case class Characteristics(
    characteristics: RawCharacteristics
  ) extends CharacterData

  final case class Fight(
    currentFightId: Int,
    isKo: Boolean,
    isDead: Boolean,
    isSummoned: Boolean,
    isFleeing: Boolean,
    obstacleId: Byte,
    SUMMONDATA: Option[FightSUMMONDATA]
  ) extends CharacterData

  final case class AccountInformation(
    adminRights: Array[Int],
    subscriptionLevel: Int,
    heroesSubscriptionLevel: Int,
    m_freeHeroesSubscriptionLevel: Int,
    forcedSubscriptionLevel: Int,
    antiAddictionLevel: Int,
    sessionStartTime: Long,
    additionalRights: Array[Int],
    additionalSlots: Byte,
    accountSecurityType: Byte,
    accountData: Array[AccountInformationAccountData]
  ) extends CharacterData

  final case class RemoteAccountInformation(
    subscriptionLevel: Int,
    additionalRights: Array[Int],
    heroesSubscriptionLevel: Int,
    characterMaxLevel: Short,
    betaReward: Long
  ) extends CharacterData

  final case class EquipmentAppearance(
    content: I8Array[EquipmentAppearanceEquipmentAppearance]
  ) extends CharacterData

  final case class Properties(
    properties: Option[PropertiesProperties]
  ) extends CharacterData

  final case class ControlledByAI(
    controlledByAI: Boolean
  ) extends CharacterData

  final case class Occupation(
    occupation: Option[OccupationOccupation]
  ) extends CharacterData

  final case class SocialStates(
    afkState: Boolean,
    dndState: Boolean
  ) extends CharacterData

  final case class Achievements(
    serializedAchievementsContext: Array[Byte]
  ) extends CharacterData

  final case class Locks(
    locks: RawLocks
  ) extends CharacterData

  final case class LocksForClient(
    content: Array[LocksForClientLocks]
  ) extends CharacterData

  final case class NPCSerializedAppearance(
    show: Boolean
  ) extends CharacterData

  final case class NPCCompanionInfo(
    controllerId: Long,
    companionId: Long
  ) extends CharacterData

  final case class NPCSerializedCharacteristics(
    level: Short,
    states: Array[Byte]
  ) extends CharacterData

  final case class NPCSerializedGroup(
    groupId: Long,
    members: Array[NPCSerializedGroupMember]
  ) extends CharacterData

  final case class NPCSerializedCollect(
    unavailableActions: Array[NPCSerializedCollectCollectAction]
  ) extends CharacterData

  final case class NPCSerializedUserTemplate(
    sightRadius: Short,
    aggroRadius: Short
  ) extends CharacterData

  final case class LandMarkInventory(
    landMarks: Array[LandMarkInventoryLandMark]
  ) extends CharacterData

  final case class AntiAddiction(
    addictionData: Option[AntiAddictionAddictionData]
  ) extends CharacterData

  final case class DungeonProgression(
    data: Array[Byte]
  ) extends CharacterData

  final case class CraftRawLearnedRecipe(
    recipeId: Int
  ) extends CharacterData

  final case class CraftRawCraft(
    refCraftId: Int,
    xp: Long,
    craftCounter: CraftCounter,
    rawLearnedRecipes: Array[CraftRawLearnedRecipe]
  ) extends CharacterData

  final case class CraftCounterPlantationCounter(
    counter: Int
  ) extends CharacterData

  final case class CraftCounterNonDestructiveCollectCounter(
    counter: Int
  ) extends CharacterData

  final case class CraftCounterDestructiveCollectCounter(
    counter: Int
  ) extends CharacterData

  final case class CraftCounterRecipeCounter(
    counter: Int
  ) extends CharacterData

  final case class PetPet(
    definitionId: Int,
    colorRefItemId: Int,
    equippedRefItemId: Int,
    sleepRefItemId: Int,
    health: Int,
    reskinRefItemId: Int
  ) extends CharacterData

  final case class PetMount(
    definitionId: Int,
    colorRefItemId: Int,
    equippedRefItemId: Int,
    sleepRefItemId: Int,
    health: Int,
    reskinRefItemId: Int
  ) extends CharacterData

  final case class NationCitizenScoreNationCitizenScoreInfo(
    nationId: Int,
    citizenScore: Int
  ) extends CharacterData

  final case class NationCitizenScoreOffendedNations(
    offendedNationId: Int
  ) extends CharacterData

  final case class DimensionalBagForSaveDimensionalBag(
    bag: RawDimensionalBagForSave
  ) extends CharacterData

  final case class PositionDimBagPosition(
    x: Int,
    y: Int,
    z: Short,
    instanceId: Short
  ) extends CharacterData

  final case class CreationDataCreationData(
    newCharacter: Boolean,
    needsRecustom: Boolean,
    recustomValue: Short,
    needInitialXp: Boolean
  ) extends CharacterData

  final case class BreedSpecificOsaSpecific(
    symbiot: RawSymbiot
  ) extends CharacterData

  final case class TitleTitleAvalability(
    availableTitle: Short
  ) extends CharacterData

  final case class EmoteInventoryEmote(
    emoteId: Int
  ) extends CharacterData

  final case class DimensionalBagViewInventoryView(
    viewId: Int
  ) extends CharacterData

  final case class DiscoveredItemsInventoryZaap(
    zaapId: Int
  ) extends CharacterData

  final case class DiscoveredItemsInventoryDrago(
    dragoId: Int
  ) extends CharacterData

  final case class DiscoveredItemsInventoryBoat(
    boatId: Int
  ) extends CharacterData

  final case class DiscoveredItemsInventoryCannon(
    cannonId: Int
  ) extends CharacterData

  final case class DiscoveredItemsInventoryPhoenix(
    phoenixId: Int
  ) extends CharacterData

  final case class StatisticsAchievementDate(
    achievementId: Int,
    date: Long
  ) extends CharacterData

  final case class StatisticsKillCounter(
    breedId: Short,
    count: Short
  ) extends CharacterData

  final case class StatisticsCollectCounter(
    referenceId: Short,
    count: Short
  ) extends CharacterData

  final case class StatisticsPlantCounter(
    referenceId: Short,
    count: Short
  ) extends CharacterData

  final case class StatisticsCraftCounter(
    referenceId: Int,
    count: Short
  ) extends CharacterData

  final case class StatisticsDropCounter(
    referenceId: Int,
    count: Short
  ) extends CharacterData

  final case class StatisticsMarketCounter(
    itemId: Int,
    count: Int,
    price: Int
  ) extends CharacterData

  final case class SpellDeckSpellDeck(
    index: Int,
    activeSpells: Array[Int],
    passiveSpells: Array[Int],
    name: String,
    level: Int
  ) extends CharacterData

  final case class ShortcutInventoriesShortcutInventory(
    inventory: RawShortcutInventory
  ) extends CharacterData

  final case class RunningEffectsInFightData(
    data: Array[Byte]
  ) extends CharacterData

  final case class RunningEffectsOutFightData(
    stateRunningEffects: RawStateRunningEffects
  ) extends CharacterData

  final case class CurrentMovementPathCurrentPath(
    encodedPath: I8Array[Byte]
  ) extends CharacterData

  final case class FightSUMMONDATA(
    summon: RawInvocationCharacteristic
  ) extends CharacterData

  final case class AccountInformationAccountData(
    id: Byte,
    value: Long
  ) extends CharacterData

  final case class EquipmentAppearanceEquipmentAppearance(
    position: Byte,
    referenceId: Int,
    skinId: Int
  ) extends CharacterData

  final case class PropertiesProperties(
    properties: RawProperties
  ) extends CharacterData

  final case class OccupationOccupation(
    occupationId: Short,
    occupationData: Array[Byte]
  ) extends CharacterData

  final case class LocksForClientLocks(
    lockId: Int,
    lockDate: Long,
    unlockDate: Long,
    currentLockValue: Int,
    currentLockValueLastModification: Long
  ) extends CharacterData

  final case class NPCSerializedGroupMember(
    breedId: Short,
    level: Short
  ) extends CharacterData

  final case class NPCSerializedCollectCollectAction(
    collectId: Int
  ) extends CharacterData

  final case class LandMarkInventoryLandMark(
    landMarkId: Byte
  ) extends CharacterData

  final case class AntiAddictionAddictionData(
    lastConnectionDate: Long,
    currentUsedQuota: Long
  ) extends CharacterData

  // useful aliases

  type WorldProperties = Properties
  type FightCharacteristics = Characteristics
  type FightProperties = Properties
  type PublicCharacteristics = Characteristics
  type LockStorage = Locks
  type AllCharacteristics = Characteristics
  type PassportInfo = PasseportInfo

  final case class RunningEffectsForGameToGame() extends CharacterData

  final case class Template() extends CharacterData

  final case class GiftTokenAccountInventory() extends CharacterData

  final case class Collect() extends CharacterData

  final case class CompanionControllerId() extends CharacterData
}
