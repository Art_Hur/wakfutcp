package wakfutcp.protocol

import com.typesafe.config.{Config, ConfigFactory}

import scala.collection.JavaConverters._

object config {
  val factory: Config = ConfigFactory.load()
  val protocol: Config = factory.getConfig("protocol")

  object client {
    val codecs: Map[Short, CodecPath] = loadCodecPaths(protocol.getConfig("client.codecs"))
  }
  object server {
    val codecs: Map[Short, CodecPath] = loadCodecPaths(protocol.getConfig("server.codecs"))
  }

  // scalastyle:off
  private def loadCodecPaths(config: Config): Map[Short, CodecPath] =
    config
      .entrySet()
      .asScala
      .map { entry =>
        val id = entry.getKey
        val codecPath = entry.getValue.unwrapped() match {
          case str: String => str
          case _ => throw new RuntimeException("Codec class path has to be a string!")
        }
        val (path, rest) = codecPath.splitAt(codecPath.lastIndexOf("."))
        val fieldName = rest.drop(1)
        if (!path.endsWith("$")) {
          throw new RuntimeException("Expecting a singleton and a field")
        }
        try {
          id.toShort -> CodecPath(path, fieldName)
        } catch {
          case _: Throwable =>
            throw new RuntimeException("Message ID has to be a valid short integer")
        }
      }.toMap
  // scalastyle:on

  final case class CodecPath(singleton: String, field: String)
}
