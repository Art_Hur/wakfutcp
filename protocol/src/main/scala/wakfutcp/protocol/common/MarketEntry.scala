package wakfutcp.protocol.common

import com.github.nscala_time.time.Imports._
import wakfutcp.protocol.Codec
import wakfutcp.protocol.raw.RawInventoryItem

final case class MarketEntry(
  id: Long,
  sellerId: Long,
  sellerName: String,
  packTypeId: Byte,
  packNumber: Short,
  packPrice: Int,
  durationId: Byte,
  releaseDate: DateTime,
  rawItem: RawInventoryItem
)

object MarketEntry {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[MarketEntry] =
    (long, long, utf8(short), byte, short, int, byte, dateTime, Codec[RawInventoryItem])
      .imapN(apply)(Function.unlift(unapply))
}
