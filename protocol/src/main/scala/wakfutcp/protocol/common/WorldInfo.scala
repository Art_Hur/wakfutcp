package wakfutcp.protocol.common

import cats.syntax.apply._
import wakfutcp.protocol.Codec

final case class ConfigProperty(
  property: Short,
  value: String
)

object ConfigProperty {
  import Codec._

  implicit val codec: Codec[ConfigProperty] =
    (short, utf8(int)).imapN(apply)(Function.unlift(unapply))
}

final case class WorldInfo(
  serverId: Int,
  version: Version.WithBuild,
  config: Array[ConfigProperty],
  locked: Boolean
)

object WorldInfo {
  import Codec._

  implicit val codec: Codec[WorldInfo] =
    (int, block(int, Version.codecWithBuild), block(int, array(int, ConfigProperty.codec)), bool)
      .imapN(apply)(Function.unlift(unapply))
}
