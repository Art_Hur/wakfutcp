package wakfutcp.protocol.common

import enumeratum.values.{IntEnum, IntEnumEntry}
import wakfutcp.protocol.Codec

import scala.collection.immutable

sealed abstract class Community(val value: Int) extends IntEnumEntry with Serializable

case object Community extends IntEnum[Community] {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[Community] =
    int.imap(withValue)(_.value)

  case object FR extends Community(0)
  case object UK extends Community(1)
  case object INT extends Community(2)
  case object DE extends Community(3)
  case object ES extends Community(4)
  case object RU extends Community(5)
  case object PT extends Community(6)
  case object NL extends Community(7)
  case object JP extends Community(8)
  case object IT extends Community(9)
  case object NA extends Community(10)
  case object CN extends Community(11)
  case object ASIA extends Community(12)
  case object TW extends Community(13)

  def values: immutable.IndexedSeq[Community] = findValues

}
