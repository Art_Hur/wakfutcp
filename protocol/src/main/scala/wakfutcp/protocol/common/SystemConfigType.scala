package wakfutcp.protocol.common

import enumeratum.values.{ShortEnum, ShortEnumEntry}
import wakfutcp.protocol.Codec

sealed abstract class SystemConfigType(val value: Short, val default: String)
    extends ShortEnumEntry
    with Serializable

case object SystemConfigType extends ShortEnum[SystemConfigType] {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[SystemConfigType] =
    short.imap(withValueOpt(_).getOrElse(Unknown))(_.value)

  case object MonitoredProperties extends SystemConfigType(1, "true")
  case object CalendarDelta extends SystemConfigType(2, "0")
  case object CalendarTz extends SystemConfigType(3, "UTC")
  case object CensorshipEnable extends SystemConfigType(4, "false")
  case object ClientCanDisableProfanityFilter extends SystemConfigType(6, "true")
  case object PlayerLevelCap extends SystemConfigType(7, "-1")
  case object KrosmozGamesEnable extends SystemConfigType(10, "true")
  case object ShopIngameInteractionsEnable extends SystemConfigType(11, "false")
  case object ContactModeratorEnable extends SystemConfigType(12, "true")
  case object DisplaySubscriptionEndPopup extends SystemConfigType(13, "true")
  case object ShopEnabled extends SystemConfigType(15, "true")
  case object MetricsReporterEnable extends SystemConfigType(19, "false")
  case object PlatformCommunity extends SystemConfigType(102, "fr")
  case object GameId extends SystemConfigType(201, "3")
  case object ExpoModeEnable extends SystemConfigType(202, "false")
  case object AdminRightsForceAll extends SystemConfigType(203, "false")
  case object ServerLockEnable extends SystemConfigType(204, "false")
  case object CommunityCheckEnable extends SystemConfigType(208, "false")
  case object AntiAddictionEnable extends SystemConfigType(211, "false")
  case object SubscriptionForcedDurationInSecond extends SystemConfigType(215, "-1")
  case object QueueActivated extends SystemConfigType(218, "true")
  case object QueuePlayerLimit extends SystemConfigType(219, "0")
  case object AuthorizedPartners extends SystemConfigType(220, "default;steam")
  case object InstanceStaticDistribution extends SystemConfigType(301, "true")
  case object AskForSecretQuestionOnCharacterDeletion extends SystemConfigType(302, "false")
  case object PreboostCharacterEnable extends SystemConfigType(401, "false")
  case object FightChallengeEnable extends SystemConfigType(404, "true")
  case object CompanionsEnable extends SystemConfigType(406, "true")
  case object BetaMode extends SystemConfigType(407, "false")
  case object SubscriptionDefaultValue extends SystemConfigType(408, "0")
  case object SubscriptionDateTimezone extends SystemConfigType(409, "Europe/Paris")
  case object HavenWorldsEnable extends SystemConfigType(410, "true")
  case object FreeCompanionEnable extends SystemConfigType(411, "true")
  case object RerollXpBonusEnable extends SystemConfigType(412, "true")
  case object ShopKey extends SystemConfigType(413, "WAKFU_INGAME")
  case object CollectFightEnabled extends SystemConfigType(416, "false")
  case object FightPremiumDisplay extends SystemConfigType(417, "false")
  case object TimerForFirstCollect extends SystemConfigType(418, "false")
  case object ForceBindOnPickup extends SystemConfigType(419, "false")
  case object ShopEnableKrosz extends SystemConfigType(421, "true")
  case object FightReworkEnabled extends SystemConfigType(422, "true")
  case object ItemTrackerLogLevel extends SystemConfigType(423, "10000")
  case object RecoInFightEnabled extends SystemConfigType(424, "false")
  case object NewHpLossFormula extends SystemConfigType(425, "true")
  case object SteamEnabled extends SystemConfigType(426, "false")
  case object VaultEnabled extends SystemConfigType(427, "true")
  case object ZaapFree extends SystemConfigType(428, "false")
  case object NewAptitudeEnabled extends SystemConfigType(429, "true")
  case object PandaNewBarrel extends SystemConfigType(430, "true")
  case object HeroesEnabled extends SystemConfigType(431, "false")
  case object VersionCheck extends SystemConfigType(500, "true")
  case object ProxyList extends SystemConfigType(600, "proxies.json")
  case object AdminList extends SystemConfigType(601, "admins.json")
  case object HeroesForceAddToParty extends SystemConfigType(602, "false")
  case object CompaniesEnabled extends SystemConfigType(603, "true")
  case object NewCraftEnabled extends SystemConfigType(604, "false")
  case object HaapiUser extends SystemConfigType(605, "web")
  case object HaapiPass extends SystemConfigType(606, "pass")
  case object HaapiURL extends SystemConfigType(607, "https://haapi.ankama.tst/json/Ankama/v2")
  case object HaapiTimeout extends SystemConfigType(608, "5000")
  case object NewChatEnabled extends SystemConfigType(609, "false")
  case object DefaultBonusSheets extends SystemConfigType(610, "0")
  case object Trunk extends SystemConfigType(611, "false")
  case object Unknown extends SystemConfigType(-1, "")

  val values = findValues
}
