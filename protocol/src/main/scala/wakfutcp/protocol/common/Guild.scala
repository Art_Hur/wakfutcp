package wakfutcp.protocol.common

import wakfutcp.protocol.common.Guild._
import wakfutcp.protocol.Codec

final case class Guild(
  id: Long,
  level: Short,
  blazon: Long,
  nationId: Int,
  name: String,
  description: String,
  message: String,
  currentPoints: Int,
  totalPoints: Int,
  weeklyPointsLimit: Int,
  earnedPointsWeekly: Int,
  lastEarningPointWeek: Int,
  ranks: Array[Rank],
  members: Array[Member],
  bonuses: Array[Bonus]
)

object Guild {
  import Codec._
  import cats.syntax.apply._

  final case class Member(
    id: Long,
    field1: Long,
    guildPoints: Int,
    rank: Long,
    xp: Long,
    connected: Boolean,
    smiley: Byte,
    sex: Byte,
    breed: Short,
    nation: Int,
    name: String
  )

  object Member {
    implicit val codec: Codec[Member] =
      (long, long, int, long, long, bool, byte, byte, short, int, utf8(int))
        .imapN(apply)(Function.unlift(unapply))
  }

  final case class Rank(
    id: Long,
    name: String,
    authorizations: Long,
    position: Short
  )

  object Rank {
    implicit val codec: Codec[Rank] =
      (long, utf8(int), long, short)
        .imapN(apply)(Function.unlift(unapply))
  }

  final case class Bonus(id: Int, activationDate: Long, buyDate: Long)

  object Bonus {
    implicit val codec: Codec[Bonus] =
      (int, long, long).imapN(apply)(Function.unlift(unapply))
  }

  implicit val codec: Codec[Guild] = (
    long,
    short,
    long,
    int,
    utf8(int),
    utf8(int),
    utf8(int),
    int,
    int,
    int,
    int,
    int,
    array(int, Rank.codec),
    array(int, Member.codec),
    array(int, Bonus.codec))
    .imapN(apply)(Function.unlift(unapply))
}
