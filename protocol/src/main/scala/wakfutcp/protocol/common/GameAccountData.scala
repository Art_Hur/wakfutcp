package wakfutcp.protocol.common

import java.nio.ByteBuffer

import akka.util.ByteStringBuilder
import wakfutcp.protocol.protobuf.equipment._
import wakfutcp.protocol.Codec

sealed trait GameAccountData extends Serializable

object GameAccountData {
  implicit val codec: Codec[GameAccountData] = new Codec[GameAccountData] {
    import wakfutcp.protocol.Encoder.DefaultOrder
    import wakfutcp.protocol.util.extensions._

    override def encode(builder: ByteStringBuilder, t: GameAccountData): ByteStringBuilder = {
      t match {
        case Characters(chars) =>
          builder.putInt(0).putInt(chars.size)
          chars.foreach {
            case (i, c) =>
              builder.putLong(i).putShort(c)
          }
          builder
        case c: Company =>
          builder
            .putLong(c.accountId)
            .putLong(c.companyChefId)
            .putInt(c.nationId)
            .putLong(c.guildId)
            .putUTF8_32(c.field5)
            .putInt(c.field6)
            .putBoolean(c.isActivated)
        case eq: EquipmentInventory =>
          eq.inventory.writeTo(builder.asOutputStream)
          builder
        case eq: EquipmentBuildSheets =>
          eq.equipment.writeTo(builder.asOutputStream)
          builder
      }
    }

    override def decode(bb: ByteBuffer): GameAccountData =
      bb.getInt match {
        case 0 =>
          Characters(
            Seq
              .fill(bb.getInt) {
                (bb.getLong, bb.getShort)
              }
              .toMap
          )
        case 1 =>
          Company(
            bb.getLong,
            bb.getLong,
            bb.getInt,
            bb.getLong,
            bb.getUTF8_32,
            bb.getInt,
            bb.get == 1
          )
        case 2 =>
          EquipmentInventory(
            ProtoEquipmentInventory.parseFrom(bb.getByteArray_32)
          )
        case 3 =>
          EquipmentBuildSheets(
            ProtoEquipmentAccount.parseFrom(bb.getByteArray_32)
          )

      }
  }

  final case class Characters(characters: Map[Long, Short]) extends GameAccountData

  final case class Company(
    accountId: Long,
    companyChefId: Long,
    nationId: Int,
    guildId: Long,
    field5: String,
    field6: Int,
    isActivated: Boolean
  ) extends GameAccountData

  final case class EquipmentInventory(inventory: ProtoEquipmentInventory) extends GameAccountData

  final case class EquipmentBuildSheets(equipment: ProtoEquipmentAccount) extends GameAccountData

}
