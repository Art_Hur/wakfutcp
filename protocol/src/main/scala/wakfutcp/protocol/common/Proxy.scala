package wakfutcp.protocol.common

import cats.syntax.apply._
import wakfutcp.protocol.Codec

final case class Proxy(
  id: Int,
  name: String,
  community: Community,
  server: ProxyServer,
  order: Byte
)

final case class ProxyServer(address: String, ports: Array[Int])

object ProxyServer {
  import Codec._

  implicit val codec: Codec[ProxyServer] =
    (utf8(int), array(int, int))
      .imapN(apply)(Function.unlift(unapply))
}

object Proxy {
  import Codec._

  implicit val codec: Codec[Proxy] =
    (int, utf8(int), Community.codec, ProxyServer.codec, byte)
      .imapN(apply)(Function.unlift(unapply))
}
