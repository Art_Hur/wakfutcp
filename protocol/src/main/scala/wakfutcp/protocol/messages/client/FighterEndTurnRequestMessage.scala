package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class FighterEndTurnRequestMessage(
  fighterId: Long,
  tableTurn: Short
) extends ClientMessage {
  override val id = 8105
  override val arch = 3
}

object FighterEndTurnRequestMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[FighterEndTurnRequestMessage] =
    (long, short).imapN(apply)(Function.unlift(unapply))
}
