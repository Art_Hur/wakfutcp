package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class FighterReadyRequestMessage(
  ready: Boolean
) extends ClientMessage {
  override val id = 8149
  override val arch = 3
}

object FighterReadyRequestMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[FighterReadyRequestMessage] =
    bool.imap(apply)(Function.unlift(unapply))
}
