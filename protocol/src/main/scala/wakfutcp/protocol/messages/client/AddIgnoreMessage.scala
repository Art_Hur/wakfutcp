package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class AddIgnoreMessage(
  ignoreName: String
) extends ClientMessage {
  override val id = 3131
  override val arch = 4
}

object AddIgnoreMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[AddIgnoreMessage] =
    utf8(byte).imap(apply)(Function.unlift(unapply))
}
