package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class ExchangeSetReadyMessage(
  exchangeId: Long
) extends ClientMessage {
  override val id = 6021
  override val arch = 3
}

object ExchangeSetReadyMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[ExchangeSetReadyMessage] =
    long.imap(apply)(Function.unlift(unapply))
}
