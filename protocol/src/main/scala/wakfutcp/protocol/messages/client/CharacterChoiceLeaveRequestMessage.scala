package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class CharacterChoiceLeaveRequestMessage() extends ClientMessage {
  override val id = 2055
  override val arch = 2
}

object CharacterChoiceLeaveRequestMessage {
  implicit val codec: Codec[CharacterChoiceLeaveRequestMessage] =
    Codec.identity(CharacterChoiceLeaveRequestMessage())
}
