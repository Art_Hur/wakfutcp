package wakfutcp.protocol.messages.client

import wakfutcp.protocol.common.Position
import wakfutcp.protocol.{ClientMessage, Codec}

final case class ItemActionRequestMessage(
  actionId: Int,
  itemId: Long,
  position: Option[Position]
) extends ClientMessage {
  override val id = 5309
  override val arch = 3
}

object ItemActionRequestMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ItemActionRequestMessage] =
    (int, long, option(Position.codec)).imapN(apply)(Function.unlift(unapply))
}
