package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class ActorRestRequestMessage() extends ClientMessage {
  override val id = 4119
  override val arch = 3
}

object ActorRestRequestMessage {
  implicit val codec: Codec[ActorRestRequestMessage] =
    Codec.identity(ActorRestRequestMessage())
}
