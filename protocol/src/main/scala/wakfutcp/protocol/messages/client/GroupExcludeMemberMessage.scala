package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class GroupExcludeMemberMessage(
  groupId: Long,
  characterId: Long
) extends ClientMessage {
  override val id = 505
  override val arch = 2
}

object GroupExcludeMemberMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[GroupExcludeMemberMessage] =
    (long, long).imapN(apply)(Function.unlift(unapply))
}
