package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class ExchangeSetCashRequestMessage(
  exchangeId: Long,
  cashInExchange: Int
) extends ClientMessage {
  override val id = 6013
  override val arch = 3
}

object ExchangeSetCashRequestMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ExchangeSetCashRequestMessage] =
    (long, int).imapN(apply)(Function.unlift(unapply))
}
