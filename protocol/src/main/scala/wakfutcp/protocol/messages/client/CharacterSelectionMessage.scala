package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class CharacterSelectionMessage(
  characterId: Long,
  characterName: String
) extends ClientMessage {
  override val id = 2049
  override val arch = 2
}

object CharacterSelectionMessage {
  import cats.syntax.apply._
  import Codec._

  implicit val codec: Codec[CharacterSelectionMessage] =
    (long, utf8(int))
      .imapN(apply)(Function.unlift(unapply))
}
