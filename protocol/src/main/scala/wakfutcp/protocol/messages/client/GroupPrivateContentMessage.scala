package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class GroupPrivateContentMessage(
  groupId: Long,
  content: String
) extends ClientMessage {
  override val id = 507
  override val arch = 6
}

object GroupPrivateContentMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[GroupPrivateContentMessage] =
    (long, utf8(byte)).imapN(apply)(Function.unlift(unapply))
}
