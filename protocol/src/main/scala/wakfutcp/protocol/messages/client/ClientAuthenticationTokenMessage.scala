package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class ClientAuthenticationTokenMessage(
  token: String
) extends ClientMessage {
  override val id = 1213
  override val arch = 1
}

object ClientAuthenticationTokenMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[ClientAuthenticationTokenMessage] =
    utf8(int).imap(apply)(Function.unlift(unapply))
}
