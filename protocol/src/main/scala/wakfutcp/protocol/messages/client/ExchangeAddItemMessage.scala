package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class ExchangeAddItemMessage(
  itemId: Long,
  exchangeId: Long,
  itemQuantity: Short
) extends ClientMessage {
  override val id = 6009
  override val arch = 3
}

object ExchangeAddItemMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ExchangeAddItemMessage] =
    (long, long, short).imapN(apply)(Function.unlift(unapply))
}
