package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class ClientPublicKeyRequestMessage(
  serverId: Byte
) extends ClientMessage {
  override val id = 1033
  override def arch: Byte = serverId
}

object ClientPublicKeyRequestMessage {
  implicit val codec: Codec[ClientPublicKeyRequestMessage] =
    Codec.identity(ClientPublicKeyRequestMessage(-1)) // TODO: do something about this
}
