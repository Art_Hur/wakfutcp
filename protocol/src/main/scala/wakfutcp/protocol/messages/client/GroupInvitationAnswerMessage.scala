package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class GroupInvitationAnswerMessage(
  groupType: Byte,
  invitationAccepted: Boolean,
  inviterName: String,
  groupName: String,
  inviterId: Long
) extends ClientMessage {
  override val id = 503
  override val arch = 2
}

object GroupInvitationAnswerMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[GroupInvitationAnswerMessage] =
    (byte, bool, utf8(byte), utf8(byte), long)
      .imapN(apply)(Function.unlift(unapply))
}
