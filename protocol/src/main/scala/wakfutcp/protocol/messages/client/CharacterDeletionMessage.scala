package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class CharacterDeletionMessage(
  characterId: Long
) extends ClientMessage {
  override val id = 2051
  override val arch = 2
}

object CharacterDeletionMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[CharacterDeletionMessage] =
    long.imap(apply)(Function.unlift(unapply))
}
