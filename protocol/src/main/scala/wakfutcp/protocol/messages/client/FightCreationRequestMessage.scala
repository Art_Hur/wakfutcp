package wakfutcp.protocol.messages.client

import wakfutcp.protocol.common.Position
import wakfutcp.protocol.{ClientMessage, Codec}

final case class FightCreationRequestMessage(
  targetId: Long,
  position: Position,
  lockInitially: Boolean
) extends ClientMessage {
  override val id = 8001
  override val arch = 3
}

object FightCreationRequestMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[FightCreationRequestMessage] =
    (long, Position.codec, bool).imapN(apply)(Function.unlift(unapply))
}
