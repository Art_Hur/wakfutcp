package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class TeleporterActionMessage(
  elementId: Long,
  exitId: Int,
  field2: Boolean,
  field3: Int
) extends ClientMessage {
  override val id = 205
  override val arch = 3
}

object TeleporterActionMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[TeleporterActionMessage] =
    (long, int, bool, int)
      .imapN(apply)(Function.unlift(unapply))
}
