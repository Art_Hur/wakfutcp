package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class MarketConsultRequestMessage(
  itemType: Short,
  minPrice: Int,
  maxPrice: Int,
  sortType: Byte,
  firstIdx: Short,
  lowestMode: Boolean,
  itemRefs: Array[Int]
) extends ClientMessage {
  override val id = 15263
  override val arch = 3
}

object MarketConsultRequestMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[MarketConsultRequestMessage] =
    (short, int, int, byte, short, bool, repeatUntilEnd(int))
      .imapN(apply)(Function.unlift(unapply))
}
