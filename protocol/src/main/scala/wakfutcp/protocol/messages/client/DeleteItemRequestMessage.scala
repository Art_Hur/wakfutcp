package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class DeleteItemRequestMessage(
  itemId: Long
) extends ClientMessage {
  override val id = 5261
  override val arch = 3
}

object DeleteItemRequestMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[DeleteItemRequestMessage] =
    long.imap(apply)(Function.unlift(unapply))
}
