package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class ActorPlayEmoteRequestMessage(
  emoteId: Int,
  emoteOccupation: Boolean,
  targetId: Long
) extends ClientMessage {
  override val id = 15401
  override val arch = 3
}

object ActorPlayEmoteRequestMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ActorPlayEmoteRequestMessage] =
    (int, bool, long).imapN(apply)(Function.unlift(unapply))
}
