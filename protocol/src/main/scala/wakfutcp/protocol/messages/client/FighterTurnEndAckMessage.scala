package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class FighterTurnEndAckMessage(
  acknowledgedTurn: Int
) extends ClientMessage {
  override val id = 8112
  override val arch = 3
}

object FighterTurnEndAckMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[FighterTurnEndAckMessage] =
    int.imap(apply)(Function.unlift(unapply))
}
