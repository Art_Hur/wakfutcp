package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class ExchangeCancelMessage(
  exchangeId: Long
) extends ClientMessage {
  override val id = 6049
  override val arch = 3
}

object ExchangeCancelMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[ExchangeCancelMessage] =
    long.imap(apply)(Function.unlift(unapply))
}
