package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class PickUpItemToItemInventoryMessage(
  interactiveElementId: Long,
  itemIds: Array[Long]
) extends ClientMessage {
  override val id = 5207
  override val arch = 3
}

object PickUpItemToItemInventoryMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[PickUpItemToItemInventoryMessage] =
    (long, array(short, long))
      .imapN(apply)(Function.unlift(unapply))
}
