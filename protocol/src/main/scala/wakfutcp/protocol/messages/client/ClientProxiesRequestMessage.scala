package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class ClientProxiesRequestMessage() extends ClientMessage {
  override val id = 1035
  override val arch = 8
}

object ClientProxiesRequestMessage {
  implicit val codec: Codec[ClientProxiesRequestMessage] =
    Codec.identity(ClientProxiesRequestMessage())
}
