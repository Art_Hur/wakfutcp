package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class InteractiveElementDespawnMessage(
  interactiveElementIds: Array[Long]
) extends ServerMessage {
  override val id = 206
}

object InteractiveElementDespawnMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[InteractiveElementDespawnMessage] =
    array(short, long).imap(apply)(Function.unlift(unapply))
}
