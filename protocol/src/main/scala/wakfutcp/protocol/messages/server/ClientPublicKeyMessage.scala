package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class ClientPublicKeyMessage(
  salt: Long,
  publicKey: Array[Byte]
) extends ServerMessage {
  override val id = 1034
}

object ClientPublicKeyMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ClientPublicKeyMessage] =
    (long, remainingBytes)
      .imapN(apply)(Function.unlift(unapply))
}
