package wakfutcp.protocol.messages.server

import wakfutcp.protocol.common.Version
import wakfutcp.protocol.{Codec, ServerMessage}

final case class ClientVersionResultMessage(
  success: Boolean,
  required: Version
) extends ServerMessage {
  override val id = 8
}

object ClientVersionResultMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ClientVersionResultMessage] =
    (bool, Version.codec)
      .imapN(apply)(Function.unlift(unapply))
}
