package wakfutcp.protocol.messages.server

import wakfutcp.protocol.common.GameAccountData
import wakfutcp.protocol.{Codec, ServerMessage}

final case class GameAccountMessage(datas: Array[GameAccountData]) extends ServerMessage {
  override val id = 5567
}

object GameAccountMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[GameAccountMessage] =
    array(int, GameAccountData.codec).imap(apply)(Function.unlift(unapply))
}
