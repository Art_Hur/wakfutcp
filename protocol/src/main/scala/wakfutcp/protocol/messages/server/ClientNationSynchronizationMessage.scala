package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class ClientNationSynchronizationMessage(
  nations: Map[Int, Array[Byte]] // TODO: codec for the nation data
) extends ServerMessage {
  override val id = 20000
}

object ClientNationSynchronizationMessage {
  import Codec._
  import cats.syntax.invariant._
  import cats.syntax.apply._

  private val entryCodec: Codec[(Int, Array[Byte])] =
    (int, byteArray(int)).imapN((_, _))(x => x)

  implicit val codec: Codec[ClientNationSynchronizationMessage] =
    repeatUntilEnd(entryCodec)
      .imap(_.toMap)(_.toArray)
      .imap(apply)(Function.unlift(unapply))
}
