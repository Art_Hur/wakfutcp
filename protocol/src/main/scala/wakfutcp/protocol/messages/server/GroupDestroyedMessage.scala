package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class GroupDestroyedMessage(
  groupId: Long
) extends ServerMessage {
  override val id = 516
}

object GroupDestroyedMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[GroupDestroyedMessage] =
    long.imap(apply)(Function.unlift(unapply))
}
