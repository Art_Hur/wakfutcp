package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class CharacterEnterPartitionMessage(worldX: Int, worldY: Int)
    extends ServerMessage {
  override val id = 4125
}

object CharacterEnterPartitionMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[CharacterEnterPartitionMessage] =
    (int, int).imapN(apply)(Function.unlift(unapply))
}
