package wakfutcp.protocol.messages.server

import wakfutcp.protocol.common.{Direction, Position}
import wakfutcp.protocol.{Codec, ServerMessage}

final case class ActorMoveToMessage(
  actorId: Long,
  position: Position,
  direction: Direction
) extends ServerMessage {
  override val id = 4127
}

object ActorMoveToMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ActorMoveToMessage] =
    (long, Position.codec, Direction.codec)
      .imapN(apply)(Function.unlift(unapply))
}
