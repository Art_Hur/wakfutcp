package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class NotificationFriendOfflineMessage(
  friendName: String,
  characterName: String,
  userId: Long
) extends ServerMessage {
  override val id = 3150
}

object NotificationFriendOfflineMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[NotificationFriendOfflineMessage] =
    (utf8(ubyte), utf8(ubyte), long)
      .imapN(apply)(Function.unlift(unapply))
}
