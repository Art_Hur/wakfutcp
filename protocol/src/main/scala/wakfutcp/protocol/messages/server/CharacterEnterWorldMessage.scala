package wakfutcp.protocol.messages.server

import wakfutcp.protocol.common.NationProtectorInfo
import wakfutcp.protocol.protobuf.dungeon.ProtoDungeon
import wakfutcp.protocol.raw.character._
import wakfutcp.protocol.{Codec, ServerMessage}

final case class CharacterEnterWorldMessage(
  characterData: ForCharacterPositionInformation,
  serializedProtectors: Array[Array[Byte]],
  serializedProtectorInfos: Array[NationProtectorInfo],
  dungeon: ProtoDungeon
) extends ServerMessage {
  override val id = 4100
}

object CharacterEnterWorldMessage {
  import cats.syntax.apply._
  import wakfutcp.protocol.Codec._

  implicit val codec: Codec[CharacterEnterWorldMessage] = (
    block(ushort, Codec[ForCharacterPositionInformation]),
    //    block(ushort, array(short, block(ushort, Codec[RawProtector]))),
    block(ushort, array(short, byteArray(ushort))),
    block(ushort, array(short, NationProtectorInfo.codec)),
    block(ushort, protobuf(ProtoDungeon)))
    .imapN(apply)(Function.unlift(unapply))
}
