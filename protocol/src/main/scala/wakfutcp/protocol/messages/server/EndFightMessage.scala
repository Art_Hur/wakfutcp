package wakfutcp.protocol.messages.server

import java.nio.ByteBuffer

import wakfutcp.protocol.{Decoder, ServerMessage}

sealed trait EndFightMessage extends ServerMessage {
  override val id = 8300
}

object EndFightMessage {
  implicit val decoder: Decoder[EndFightMessage] = (bb: ByteBuffer) => {
    val fightId = bb.getLong
    bb.getInt
    bb.getInt
    bb.get match {
      case 0 ⇒
        Finished(
          fightId,
          Array.fill(bb.get.toInt)(bb.getLong),
          Array.fill(bb.get.toInt)(bb.getLong),
          Array.fill(bb.get.toInt)(bb.getLong)
        )
      case 1 ⇒ Fleed(fightId)
    }
  }

  final case class Finished(
    fightId: Long,
    winnerTeammates: Array[Long],
    looserTeammates: Array[Long],
    escapees: Array[Long]
  ) extends EndFightMessage

  final case class Fleed(
    fightId: Long
  ) extends EndFightMessage

}
