package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class ClientAdditionalCharacterSlotsUpdateMessage(
  additionalSlots: Byte
) extends ServerMessage {
  override val id = 2069
}

object ClientAdditionalCharacterSlotsUpdateMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[ClientAdditionalCharacterSlotsUpdateMessage] =
    byte.imap(apply)(Function.unlift(unapply))
}
