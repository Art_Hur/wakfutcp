package wakfutcp.protocol.messages.server

import wakfutcp.protocol.protobuf.equipment.ProtoEquipmentAccount
import wakfutcp.protocol.{Codec, ServerMessage}

final case class EquipmentAccountMessage(inventory: ProtoEquipmentAccount)
    extends ServerMessage {
  override val id = 5255
}

object EquipmentAccountMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[EquipmentAccountMessage] =
    block(int, protobuf(ProtoEquipmentAccount))
      .imap(apply)(Function.unlift(unapply))
}
