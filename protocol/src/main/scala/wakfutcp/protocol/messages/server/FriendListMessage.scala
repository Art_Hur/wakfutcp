package wakfutcp.protocol.messages.server

import cats.syntax.apply._
import cats.syntax.invariant._
import wakfutcp.protocol.{Codec, ServerMessage}

final case class FriendListMessage(friends: Array[Friend]) extends ServerMessage {
  override val id = 3144
}

final case class Friend(
  name: String,
  characterName: String,
  commentary: String,
  doNotify: Boolean,
  userId: Long,
  breedId: Short,
  sex: Byte,
  xp: Long
)

object Friend {
  import Codec._

  implicit val codec: Codec[Friend] =
    (utf8(ubyte), utf8(ubyte), utf8(ubyte), bool, long, short, byte, long)
      .imapN(apply)(Function.unlift(unapply))
}

object FriendListMessage {
  import Codec._

  implicit val codec: Codec[FriendListMessage] =
    array(ubyte, block(short, Friend.codec))
      .imap(apply)(Function.unlift(unapply))
}
