package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class ClientIpMessage(bytes: Array[Byte]) extends ServerMessage {
  override val id = 110
}

object ClientIpMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[ClientIpMessage] =
    byteArrayC(4).imap(apply)(Function.unlift(unapply))
}
