package wakfutcp.protocol.messages.server

import java.nio.ByteBuffer

import wakfutcp.protocol.common.Guild.{Bonus, Member, Rank}
import wakfutcp.protocol.util.extensions._
import wakfutcp.protocol.{Decoder, ServerMessage}

import scala.collection.mutable

final case class GuildChangeMessage(
  characterId: Long,
  changes: Seq[GuildChange]
) extends ServerMessage {
  override val id = 20050
}

sealed trait GuildChange

object GuildChange {
  implicit val decoder: Decoder[GuildChange] = (bb: ByteBuffer) =>
    bb.get match {
      case 0 =>
        AddMember(Decoder[Member].decode(bb))
      case 1 =>
        RemoveMember(bb.getLong)
      case 2 =>
        ModifyMember(Decoder[Member].decode(bb))
      case 3 =>
        AddBonus(Decoder[Bonus].decode(bb))
      case 4 =>
        RemoveBonus(bb.getInt)
      case 5 =>
        ModifyBonus(Decoder[Bonus].decode(bb))
      case 6 =>
        AddRank(Decoder[Rank].decode(bb))
      case 7 =>
        RemoveRank(bb.getLong)
      case 8 =>
        ModifyRank(Decoder[Rank].decode(bb))
      case 9 =>
        MoveRank(Decoder[Rank].decode(bb))
      case 10 =>
        GuildPoints(bb.getInt)
      case 11 =>
        GuildLevel(bb.getShort)
      case 12 =>
        Description(bb.getUTF8_32)
      case 13 =>
        Message(bb.getUTF8_32)
      case 14 =>
        Nation(bb.getInt)
      case 15 =>
        Name(bb.getUTF8_32)
  }

  final case class AddMember(data: Member) extends GuildChange

  final case class RemoveMember(id: Long) extends GuildChange

  final case class ModifyMember(data: Member) extends GuildChange

  final case class AddBonus(bonus: Bonus) extends GuildChange

  final case class RemoveBonus(id: Int) extends GuildChange

  final case class ModifyBonus(data: Bonus) extends GuildChange

  final case class AddRank(data: Rank) extends GuildChange

  final case class RemoveRank(id: Long) extends GuildChange

  final case class ModifyRank(data: Rank) extends GuildChange

  final case class MoveRank(data: Rank) extends GuildChange

  final case class GuildPoints(points: Int) extends GuildChange

  final case class GuildLevel(level: Short) extends GuildChange

  final case class Description(description: String) extends GuildChange

  final case class Message(message: String) extends GuildChange

  final case class Nation(nationId: Int) extends GuildChange

  final case class Name(name: String) extends GuildChange

}

object GuildChangeMessage {
  implicit val decoder: Decoder[GuildChangeMessage] = (bb: ByteBuffer) => {
    val buffer = mutable.ListBuffer[GuildChange]()
    val id = bb.getLong
    while (bb.hasRemaining) buffer += Decoder[GuildChange].decode(bb)
    GuildChangeMessage(
      id,
      buffer
    )
  }
}
