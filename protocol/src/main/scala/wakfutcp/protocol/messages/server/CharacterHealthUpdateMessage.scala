package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class CharacterHealthUpdateMessage(
  characterId: Long,
  health: Int,
  healthRegen: Int
) extends ServerMessage {
  override val id = 4124
}

object CharacterHealthUpdateMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[CharacterHealthUpdateMessage] =
    (long, int, int).imapN(apply)(Function.unlift(unapply))
}
