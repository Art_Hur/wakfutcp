package wakfutcp.protocol.messages.server

import java.nio.ByteBuffer

import wakfutcp.protocol.common.MarketEntry
import wakfutcp.protocol.{Decoder, ServerMessage}

final case class MarketConsultResultMessage(
  sales: Array[MarketEntry],
  totalCount: Int
) extends ServerMessage {
  override val id = 20100
}

object MarketConsultResultMessage {
  import wakfutcp.protocol.util.extensions._

  implicit val decoder: Decoder[MarketConsultResultMessage] = (bb: ByteBuffer) => {
    bb.get match {
      case 0 ⇒
        MarketConsultResultMessage(
          bb.getBlockUsing_32 { block =>
            Array.fill(block.getInt)(Decoder[MarketEntry].decode(block))
          },
          bb.getInt
        )
      case 1 ⇒ ???
    }
  }
}
