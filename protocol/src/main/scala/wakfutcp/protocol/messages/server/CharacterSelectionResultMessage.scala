package wakfutcp.protocol.messages.server

import enumeratum.values.{ByteEnum, ByteEnumEntry}
import wakfutcp.protocol.{Codec, ServerMessage}

import scala.collection.immutable

sealed abstract class CharacterSelectionResultMessage(val value: Byte)
    extends ByteEnumEntry
    with ServerMessage {
  override val id = 2050
}

object CharacterSelectionResultMessage extends ByteEnum[CharacterSelectionResultMessage] {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[CharacterSelectionResultMessage] =
    byte.imap(withValueOpt(_).getOrElse(Failure))(_.value)

  case object Success extends CharacterSelectionResultMessage(0)
  case object Failure extends CharacterSelectionResultMessage(1)

  def values: immutable.IndexedSeq[CharacterSelectionResultMessage] = findValues
}
