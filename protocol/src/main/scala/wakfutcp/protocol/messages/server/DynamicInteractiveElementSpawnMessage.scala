package wakfutcp.protocol.messages.server

import wakfutcp.protocol.common.InteractiveElementData
import wakfutcp.protocol.messages.server.DynamicInteractiveElementSpawnMessage._
import wakfutcp.protocol.{Codec, ServerMessage}

final case class DynamicInteractiveElementSpawnMessage(
  elements: Array[InteractiveElement]
) extends ServerMessage {
  override val id = 204
}

object DynamicInteractiveElementSpawnMessage {
  import Codec._
  import cats.syntax.apply._
  import cats.syntax.invariant._

  implicit val codec: Codec[DynamicInteractiveElementSpawnMessage] =
    array(short, InteractiveElement.codec)
      .imap(apply)(Function.unlift(unapply))

  final case class InteractiveElement(
    id: Long,
    templateId: Long,
    data: Seq[InteractiveElementData],
    position: Long
  )

  object InteractiveElement {
    implicit val codec: Codec[InteractiveElement] =
      (long, long, block(short, InteractiveElementData.codec), long)
        .imapN(apply)(Function.unlift(unapply))
  }
}
