package wakfutcp.protocol.messages.server

import wakfutcp.protocol.messages.server.ActorDespawnMessage.DespawnedActor
import wakfutcp.protocol.{Codec, ServerMessage}

final case class ActorDespawnMessage(
  applyApsOnDespawn: Boolean,
  fightDespawn: Boolean,
  actors: Array[DespawnedActor]
) extends ServerMessage {
  override val id = 4104
}

object ActorDespawnMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ActorDespawnMessage] =
    (bool, bool, array(ubyte, DespawnedActor.codec))
      .imapN(apply)(Function.unlift(unapply))

  final case class DespawnedActor(tpe: Byte, id: Long)

  object DespawnedActor {
    implicit val codec: Codec[DespawnedActor] =
      (byte, long).imapN(apply)(Function.unlift(unapply))
  }
}
