package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class HasModerationRequestMessage(
  hasRequests: Boolean
) extends ServerMessage {
  override val id = 3222
}

object HasModerationRequestMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[HasModerationRequestMessage] =
    bool.imap(apply)(Function.unlift(unapply))
}
