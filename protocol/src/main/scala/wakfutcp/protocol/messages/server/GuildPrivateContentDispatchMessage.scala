package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class GuildPrivateContentDispatchMessage(
  groupId: Long,
  senderName: String,
  senderId: Long,
  content: String
) extends ServerMessage {
  override val id = 528
}

object GuildPrivateContentDispatchMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[GuildPrivateContentDispatchMessage] =
    (long, utf8(short), long, utf8(short))
      .imapN(apply)(Function.unlift(unapply))
}
