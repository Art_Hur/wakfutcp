package wakfutcp.protocol.messages.server

import java.nio.ByteBuffer

import wakfutcp.protocol.common.Position
import wakfutcp.protocol.{Decoder, ServerMessage}

final case class FighterMoveMessage(
  fightId: Long,
  fighterId: Long,
  direction: Byte,
  position: Position,
  movementType: Byte
) extends ServerMessage {
  override val id = 4524
}

object FighterMoveMessage {
  implicit val decoder: Decoder[FighterMoveMessage] = (bb: ByteBuffer) => {
    val fightId = bb.getLong
    bb.getInt
    bb.getInt
    val fighterId = bb.getLong
    val direction = bb.get
    val movementType = bb.get
    val skip = bb.limit - 22
    bb.position(bb.position + skip - 10)
    FighterMoveMessage(
      fightId,
      fighterId,
      direction,
      Decoder[Position].decode(bb),
      movementType
    )
  }
}
