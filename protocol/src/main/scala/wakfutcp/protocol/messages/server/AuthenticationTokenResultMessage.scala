package wakfutcp.protocol.messages.server

import java.nio.ByteBuffer

import akka.util.ByteStringBuilder
import wakfutcp.protocol.{Codec, ServerMessage}

sealed trait AuthenticationTokenResultMessage extends ServerMessage {
  override val id = 1212
}

object AuthenticationTokenResultMessage {
  implicit val codec: Codec[AuthenticationTokenResultMessage] =
    new Codec[AuthenticationTokenResultMessage] {
      import wakfutcp.protocol.util.extensions._

      override def decode(bb: ByteBuffer): AuthenticationTokenResultMessage =
        bb.get match {
          case 0 ⇒
            Success(bb.getUTF8_32)
          case _ ⇒
            Failure
        }

      override def encode(builder: ByteStringBuilder,
                          t: AuthenticationTokenResultMessage): ByteStringBuilder = {
        t match {
          case Success(tok) =>
            builder.putByte(0).putUTF8_32(tok)
          case Failure =>
            builder.putByte(1)
        }
      }
    }

  final case class Success(token: String) extends AuthenticationTokenResultMessage

  final case object Failure extends AuthenticationTokenResultMessage

}
