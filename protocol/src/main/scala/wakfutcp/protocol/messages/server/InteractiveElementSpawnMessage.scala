package wakfutcp.protocol.messages.server

import wakfutcp.protocol.common.InteractiveElementData
import wakfutcp.protocol.messages.server.InteractiveElementSpawnMessage.InteractiveElement
import wakfutcp.protocol.{Codec, ServerMessage}

final case class InteractiveElementSpawnMessage(
  interactiveElements: Array[InteractiveElement]
) extends ServerMessage {
  override val id = 204
}

object InteractiveElementSpawnMessage {
  import Codec._
  import cats.syntax.apply._
  import cats.syntax.invariant._

  implicit val codec: Codec[InteractiveElementSpawnMessage] =
    array(short, InteractiveElement.codec)
      .imap(apply)(Function.unlift(unapply))

  final case class InteractiveElement(id: Long, data: Seq[InteractiveElementData])

  object InteractiveElement {
    implicit val codec: Codec[InteractiveElement] =
      (long, block(short, InteractiveElementData.codec))
        .imapN(apply)(Function.unlift(unapply))
  }
}
