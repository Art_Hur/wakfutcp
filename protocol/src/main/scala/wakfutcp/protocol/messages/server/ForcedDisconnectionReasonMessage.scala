package wakfutcp.protocol.messages.server

import enumeratum.values.{ByteEnum, ByteEnumEntry}
import wakfutcp.protocol.{Codec, ServerMessage}

import scala.collection.immutable

sealed abstract class ForcedDisconnectionReasonMessage(val value: Byte)
    extends ByteEnumEntry
    with ServerMessage {
  override val id = 6
}

object ForcedDisconnectionReasonMessage extends ByteEnum[ForcedDisconnectionReasonMessage] {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[ForcedDisconnectionReasonMessage] =
    byte.imap(withValueOpt(_).getOrElse(Unknown))(_.value)

  case object Spam extends ForcedDisconnectionReasonMessage(1)
  case object Timeout extends ForcedDisconnectionReasonMessage(2)
  case object KickedByReconnect extends ForcedDisconnectionReasonMessage(3)
  case object KickedByAdmin extends ForcedDisconnectionReasonMessage(4)
  case object AccountBanned extends ForcedDisconnectionReasonMessage(5)
  case object BannedByAdmin extends ForcedDisconnectionReasonMessage(6)
  case object SessionDestroyed extends ForcedDisconnectionReasonMessage(7)
  case object ServerDoesNotAnswer extends ForcedDisconnectionReasonMessage(8)
  case object ServerShutdown extends ForcedDisconnectionReasonMessage(9)
  case object ServerError extends ForcedDisconnectionReasonMessage(12)
  case object SynchronizationError extends ForcedDisconnectionReasonMessage(14)
  case object TimedSessionEnd extends ForcedDisconnectionReasonMessage(16)
  case object ServerFull extends ForcedDisconnectionReasonMessage(17)
  case object Unknown extends ForcedDisconnectionReasonMessage(-1)

  def values: immutable.IndexedSeq[ForcedDisconnectionReasonMessage] = findValues

}
