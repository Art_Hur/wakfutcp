package wakfutcp.protocol.messages.server

import wakfutcp.protocol.protobuf.equipment.ProtoEquipmentInventory
import wakfutcp.protocol.{Codec, ServerMessage}

final case class EquipmentInventoryMessage(inventory: ProtoEquipmentInventory)
    extends ServerMessage {
  override val id = 5256
}

object EquipmentInventoryMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[EquipmentInventoryMessage] =
    block(int, protobuf(ProtoEquipmentInventory))
      .imap(apply)(Function.unlift(unapply))
}
