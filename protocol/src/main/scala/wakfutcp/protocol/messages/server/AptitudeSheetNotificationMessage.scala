package wakfutcp.protocol.messages.server

import wakfutcp.protocol.protobuf.aptitude.SheetSet
import wakfutcp.protocol.{Codec, ServerMessage}

final case class AptitudeSheetNotificationMessage(
  characterId: Long,
  sheets: SheetSet
) extends ServerMessage {
  override val id = 8418
}

object AptitudeSheetNotificationMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[AptitudeSheetNotificationMessage] =
    (long, block(int, protobuf(SheetSet)))
      .imapN(apply)(Function.unlift(unapply))
}
