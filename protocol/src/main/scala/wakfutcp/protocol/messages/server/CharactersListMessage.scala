package wakfutcp.protocol.messages.server

import wakfutcp.protocol.raw.character._
import wakfutcp.protocol.{Codec, ServerMessage}

final case class CharactersListMessage(
  characters: Array[ForCharacterList]
) extends ServerMessage {
  override val id = 2048
}

object CharactersListMessage {
  import cats.syntax.invariant._
  import wakfutcp.protocol.Codec._

  implicit val codec: Codec[CharactersListMessage] =
    array(byte, block(short, Codec[ForCharacterList]))
      .imap(apply)(Function.unlift(unapply))
}

