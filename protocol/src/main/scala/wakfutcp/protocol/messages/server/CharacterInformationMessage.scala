package wakfutcp.protocol.messages.server

import wakfutcp.protocol.protobuf.aptitude.SheetSet
import wakfutcp.protocol.protobuf.buildSheet.ProtoBuildSheetSet
import wakfutcp.protocol.raw.character._
import wakfutcp.protocol.{Codec, ServerMessage}

final case class CharacterInformationMessage(
  reservedIds: Array[Long],
  info: ForLocalCharacterInformation,
  builds: ProtoBuildSheetSet,
  sheets: SheetSet
) extends ServerMessage {
  override val id = 4098
}

object CharacterInformationMessage {
  import cats.syntax.apply._
  import wakfutcp.protocol.Codec._

  implicit val codec: Codec[CharacterInformationMessage] =
    (array(short, long),
      block(int, Codec[ForLocalCharacterInformation]),
      block(int, protobuf(ProtoBuildSheetSet)),
      block(int, protobuf(SheetSet)))
      .imapN(apply)(Function.unlift(unapply))
}
