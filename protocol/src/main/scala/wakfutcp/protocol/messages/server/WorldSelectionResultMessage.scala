package wakfutcp.protocol.messages.server

import enumeratum.values.{ByteEnum, ByteEnumEntry}
import wakfutcp.protocol.{Codec, ServerMessage}

import scala.collection.immutable

sealed abstract class WorldSelectionResultMessage(val value: Byte)
    extends ByteEnumEntry
    with ServerMessage {
  override val id = 1202
}

object WorldSelectionResultMessage extends ByteEnum[WorldSelectionResultMessage] {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[WorldSelectionResultMessage] =
    byte.imap(withValueOpt(_).getOrElse(Failure))(_.value)

  case object Success extends WorldSelectionResultMessage(0)
  case object Failure extends WorldSelectionResultMessage(1)

  def values: immutable.IndexedSeq[WorldSelectionResultMessage] = findValues
}
