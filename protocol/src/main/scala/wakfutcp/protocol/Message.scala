package wakfutcp.protocol

sealed trait Message {
  def id: Short
}

trait ClientMessage extends Message {
  def arch: Byte
}

trait ServerMessage extends Message

object ClientMessage {
  val HeaderSize: Int = 5
}

object ServerMessage {
  val HeaderSize: Int = 4
}
